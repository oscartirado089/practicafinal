
const firebaseConfig = {
    apiKey: "AIzaSyBdeVeWnQJFkLrRSyYqj2JHbYxb4Nlgrrg",
    authDomain: "administrador-21593.firebaseapp.com",
    projectId: "administrador-21593",
    storageBucket: "administrador-21593.appspot.com",
    messagingSenderId: "156388192080",
    appId: "1:156388192080:web:c12e631d00b929257e4655"
  };

firebase.initializeApp(firebaseConfig);

  const storage = firebase.storage();
  const storageRef = storage.ref();

  function subirImagen() {
    const fileInput = document.getElementById('fileInput');
    const archivo = fileInput.files[0];
    const urlInput = document.getElementById('urlInput');

    if (archivo) {

        if (archivo.type === 'image/jpeg' || archivo.type === 'image/png') {
            const imagenRef = storageRef.child(`imagenes/${archivo.name}`);


            imagenRef.put(archivo).then(() => {
                alert('Imagen subida con éxito.');


                imagenRef.getDownloadURL().then((url) => {
                    const imagenContainer = document.getElementById('imagenContainer');
                    const imagen = document.createElement('img');
                    imagen.src = url;
                    imagen.style.maxWidth = '200px';
                    imagen.style.maxHeight = '200px';
                    imagen.style.marginRight = '10px';
                    imagenContainer.appendChild(imagen);


                    urlInput.value = url;
                }).catch((error) => {
                    alert('Error al obtener la URL de descarga: ' + error.message);
                });
            }).catch((error) => {
                alert('Error al subir la imagen: ' + error.message);
            });
        } else {
            alert('Por favor, selecciona una imagen en formato .jpg, .jpeg o .png.');
        }
    } else {
        alert('Por favor, selecciona un archivo.');
    }
}