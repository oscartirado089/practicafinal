import { initializeApp } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-app.js";
import { getAuth, signInWithEmailAndPassword } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-auth.js";

const firebaseConfig = {
  apiKey: "AIzaSyBdeVeWnQJFkLrRSyYqj2JHbYxb4Nlgrrg",
  authDomain: "administrador-21593.firebaseapp.com",
  projectId: "administrador-21593",
  storageBucket: "administrador-21593.appspot.com",
  messagingSenderId: "156388192080",
  appId: "1:156388192080:web:c12e631d00b929257e4655"
};

const app = initializeApp(firebaseConfig);


const btnEnviar = document.getElementById('btnEnviar');


btnEnviar.addEventListener("click", function(event) {
  event.preventDefault();


  const email = document.getElementById('txtEmail').value;
  const password = document.getElementById('txtPassword').value;


  const auth = getAuth();

 
  signInWithEmailAndPassword(auth, email, password)
    .then((userCredential) => {

      const user = userCredential.user;
      alert(`Usuario autenticado:\nBienvenido ${user.email}`);

      window.location.href = "/html/menu.html";
    })
    .catch((error) => {

      const errorCode = error.code;
      const errorMessage = error.message;
      alert(`El ususario/contraseña es incorrecto o no existe. Intentalo de nuevo.`);
   });
   
});
